role = Role.find_by_name_role("Superadmin")
if role.blank?
  role = Role.new(name_role: "Superadmin")
  role.save
end

user = User.find_by_username("fadhlir")
if user.blank?
  user = User.new(email: "fadhlirrahman48@gmail.com", password: "rahman", username: "fadhlir", fullname: "Fahlir Rahman", role_code:role.id)
  user.save
else
  user.role_code = role.id
  user.save
end

Menu.destroy_all
Menu.create!([
    {menu_name: "Dashboard"},
    {menu_name: "Create Transaction"},
    {menu_name: "List Transaction"},
    {menu_name: "Barang In Progress"},
    {menu_name: "Barang Completed"},
    {menu_name: "Create Invoice"},
    {menu_name: "List Invoice"},
    {menu_name: "History Inventory"},
    {menu_name: "Master Customer"},
    {menu_name: "Master Inventory"},
    {menu_name: "Master User"},
    {menu_name: "Role User"},
    {menu_name: "Print Twice"},
    {menu_name: "Inventory Sampah"},
  ])

Menu.create!(menu_name: "Master Jenis Proses")

menus_id = Menu.pluck(:id)
menus_id.each do |menu|
  RoleUser.create(role_id: role.id, menu_id: menu)
end