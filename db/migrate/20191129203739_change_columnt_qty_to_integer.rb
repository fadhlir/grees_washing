class ChangeColumntQtyToInteger < ActiveRecord::Migration[5.2]
  def change
     change_column :inventories, :qty, 'integer USING CAST(qty AS integer)'
  end
end
