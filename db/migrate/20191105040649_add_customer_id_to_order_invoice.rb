class AddCustomerIdToOrderInvoice < ActiveRecord::Migration[5.2]
  def change
    add_column :order_invoices, :customer_id, :integer
  end
end
