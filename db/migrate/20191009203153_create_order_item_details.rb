class CreateOrderItemDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :order_item_details do |t|
      t.string :invoice_number
      t.datetime :date_entry
      t.integer :item_id
      t.integer :qty
      t.string :no_si_out
      t.integer :process_id
      t.integer :unit_price
      t.integer :total_price
      t.datetime :date_target_send
      t.string :status_order_item

      t.timestamps
    end
  end
end
