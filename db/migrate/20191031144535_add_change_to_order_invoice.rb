class AddChangeToOrderInvoice < ActiveRecord::Migration[5.2]
  def change
    add_column :order_invoices, :change, :integer
  end
end
