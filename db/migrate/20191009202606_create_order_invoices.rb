class CreateOrderInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :order_invoices do |t|
      t.string :customer_name
      t.datetime :invoice_date
      t.string :invoice_number
      t.integer :total_item
      t.integer :total_qty_item
      t.integer :total_invoice
      t.datetime :due_payment_date
      t.integer :cash
      t.integer :left_payment
      t.datetime :pay_date
      t.string :status_payment

      t.timestamps
    end
  end
end
