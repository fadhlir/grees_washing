class AddPrintCount < ActiveRecord::Migration[5.2]
  def change
    add_column :order_invoices, :print_count, :integer
    add_column :invoice_masters, :print_count, :integer
  end
end
