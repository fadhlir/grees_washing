class CreateInventoryHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :inventory_histories do |t|
      t.integer :inventory_id
      t.integer :qty_in
      t.datetime :date_in
      t.datetime :date_out
      t.string :type
      t.string :employee_name
      t.integer :created_by

      t.timestamps
    end
  end
end
