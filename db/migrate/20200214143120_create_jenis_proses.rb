class CreateJenisProses < ActiveRecord::Migration[5.2]
  def change
    create_table :jenis_proses do |t|
      t.string :jenisproses_code
      t.string :jenisproses_name
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end
end
