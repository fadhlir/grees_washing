class ChangeRoleCodeToInteger < ActiveRecord::Migration[5.2]
  def change
    change_column :users, :role_code, 'integer USING CAST(role_code AS integer)'
  end
end
