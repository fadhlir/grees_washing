class ChangeColumnTypeToHistoryTypeInInventoryHistory < ActiveRecord::Migration[5.2]
  def change
    rename_column :inventory_histories, :type, :history_type
  end
end
