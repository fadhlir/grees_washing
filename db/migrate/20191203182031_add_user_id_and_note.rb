class AddUserIdAndNote < ActiveRecord::Migration[5.2]
  def change
    add_column :order_invoices, :created_by, :integer
    add_column :order_item_details, :created_by, :integer
    add_column :order_item_details, :note, :string
  end
end
