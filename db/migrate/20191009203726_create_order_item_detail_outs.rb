class CreateOrderItemDetailOuts < ActiveRecord::Migration[5.2]
  def change
    create_table :order_item_detail_outs do |t|
      t.integer :id_order_item_detail
      t.integer :no_si_out
      t.integer :qty_out
      t.datetime :date_out
      t.integer :qty_left_item
      t.integer :qty_done_item
      t.integer :total_price_left_item
      t.integer :total_price_done_item
      t.string :status_send

      t.timestamps
    end
  end
end
