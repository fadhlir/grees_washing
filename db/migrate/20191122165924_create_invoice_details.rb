class CreateInvoiceDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :invoice_details do |t|
      t.string :invoice_master_number
      t.integer :invoice_master_id
      t.integer :order_invoice_id

      t.timestamps
    end
  end
end
