class CreateInvoiceMasters < ActiveRecord::Migration[5.2]
  def change
    create_table :invoice_masters do |t|
      t.string :invoice_master_code
      t.integer :customer_id
      t.integer :total_invoice
      t.string :status_invoice
      t.integer :created_user_id

      t.timestamps
    end
  end
end
