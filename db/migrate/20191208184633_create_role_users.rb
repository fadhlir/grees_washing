class CreateRoleUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :role_users do |t|
      t.integer :user_id
      t.integer :role_id
      t.integer :menu_id

      t.timestamps
    end
  end
end
