class AddProductNameAndProcessNameToOrderInvoiceDetail < ActiveRecord::Migration[5.2]
  def change
    add_column :order_item_details, :product_name, :string
    add_column :order_item_details, :process_name, :string
  end
end
