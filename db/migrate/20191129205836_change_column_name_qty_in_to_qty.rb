class ChangeColumnNameQtyInToQty < ActiveRecord::Migration[5.2]
  def change
    rename_column :inventory_histories, :qty_in, :qty
    rename_column :inventory_histories, :date_in, :history_date
    remove_column :inventory_histories, :date_out
  end
end
