class CreateInventories < ActiveRecord::Migration[5.2]
  def change
    create_table :inventories do |t|
      t.string :name
      t.string :code
      t.string :qty
      t.integer :user_id

      t.timestamps
    end
  end
end
