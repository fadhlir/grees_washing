class CreateTrashRecordings < ActiveRecord::Migration[5.2]
  def change
    create_table :trash_recordings do |t|
      t.datetime :trash_date
      t.integer :inventory_id
      t.integer :created_by
      t.integer :qty
      t.string :status
      t.boolean :is_deleted

      t.timestamps
    end
  end
end
