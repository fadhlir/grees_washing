# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_14_143120) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "customers", force: :cascade do |t|
    t.string "customer_code"
    t.string "customer_name"
    t.string "address"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "inventories", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.integer "qty"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "inventory_histories", force: :cascade do |t|
    t.integer "inventory_id"
    t.integer "qty"
    t.datetime "history_date"
    t.string "history_type"
    t.string "employee_name"
    t.integer "created_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "invoice_details", force: :cascade do |t|
    t.string "invoice_master_number"
    t.integer "invoice_master_id"
    t.integer "order_invoice_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "invoice_masters", force: :cascade do |t|
    t.string "invoice_master_code"
    t.integer "customer_id"
    t.integer "total_invoice"
    t.string "status_invoice"
    t.integer "created_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "print_count"
  end

  create_table "jenis_proses", force: :cascade do |t|
    t.string "jenisproses_code"
    t.string "jenisproses_name"
    t.integer "created_by"
    t.integer "updated_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "menus", force: :cascade do |t|
    t.string "menu_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "order_invoices", force: :cascade do |t|
    t.string "customer_name"
    t.datetime "invoice_date"
    t.string "invoice_number"
    t.integer "total_item"
    t.integer "total_qty_item"
    t.integer "total_invoice"
    t.datetime "due_payment_date"
    t.integer "cash"
    t.integer "left_payment"
    t.datetime "pay_date"
    t.string "status_payment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "change"
    t.integer "customer_id"
    t.integer "created_by"
    t.integer "print_count"
  end

  create_table "order_item_detail_outs", force: :cascade do |t|
    t.integer "id_order_item_detail"
    t.integer "no_si_out"
    t.integer "qty_out"
    t.datetime "date_out"
    t.integer "qty_left_item"
    t.integer "qty_done_item"
    t.integer "total_price_left_item"
    t.integer "total_price_done_item"
    t.string "status_send"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "order_item_details", force: :cascade do |t|
    t.string "invoice_number"
    t.datetime "date_entry"
    t.integer "item_id"
    t.integer "qty"
    t.string "no_si_out"
    t.integer "process_id"
    t.integer "unit_price"
    t.integer "total_price"
    t.datetime "date_target_send"
    t.string "status_order_item"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "product_name"
    t.string "process_name"
    t.integer "created_by"
    t.string "note"
  end

  create_table "role_users", force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.integer "menu_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name_role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "trash_recordings", force: :cascade do |t|
    t.datetime "trash_date"
    t.integer "inventory_id"
    t.integer "created_by"
    t.integer "qty"
    t.string "status"
    t.boolean "is_deleted"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.string "username"
    t.string "fullname"
    t.integer "role_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
