require 'test_helper'

class TrashRecordingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @trash_recording = trash_recordings(:one)
  end

  test "should get index" do
    get trash_recordings_url
    assert_response :success
  end

  test "should get new" do
    get new_trash_recording_url
    assert_response :success
  end

  test "should create trash_recording" do
    assert_difference('TrashRecording.count') do
      post trash_recordings_url, params: { trash_recording: { created_by: @trash_recording.created_by, inventory_id: @trash_recording.inventory_id, is_deleted: @trash_recording.is_deleted, qty: @trash_recording.qty, status: @trash_recording.status, trash_date: @trash_recording.trash_date } }
    end

    assert_redirected_to trash_recording_url(TrashRecording.last)
  end

  test "should show trash_recording" do
    get trash_recording_url(@trash_recording)
    assert_response :success
  end

  test "should get edit" do
    get edit_trash_recording_url(@trash_recording)
    assert_response :success
  end

  test "should update trash_recording" do
    patch trash_recording_url(@trash_recording), params: { trash_recording: { created_by: @trash_recording.created_by, inventory_id: @trash_recording.inventory_id, is_deleted: @trash_recording.is_deleted, qty: @trash_recording.qty, status: @trash_recording.status, trash_date: @trash_recording.trash_date } }
    assert_redirected_to trash_recording_url(@trash_recording)
  end

  test "should destroy trash_recording" do
    assert_difference('TrashRecording.count', -1) do
      delete trash_recording_url(@trash_recording)
    end

    assert_redirected_to trash_recordings_url
  end
end
