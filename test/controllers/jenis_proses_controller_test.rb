require 'test_helper'

class JenisProsesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @jenis_prose = jenis_proses(:one)
  end

  test "should get index" do
    get jenis_proses_url
    assert_response :success
  end

  test "should get new" do
    get new_jenis_prose_url
    assert_response :success
  end

  test "should create jenis_prose" do
    assert_difference('JenisProse.count') do
      post jenis_proses_url, params: { jenis_prose: { created_by: @jenis_prose.created_by, jenisproses_code: @jenis_prose.jenisproses_code, jenisproses_name: @jenis_prose.jenisproses_name, updated_by: @jenis_prose.updated_by } }
    end

    assert_redirected_to jenis_prose_url(JenisProse.last)
  end

  test "should show jenis_prose" do
    get jenis_prose_url(@jenis_prose)
    assert_response :success
  end

  test "should get edit" do
    get edit_jenis_prose_url(@jenis_prose)
    assert_response :success
  end

  test "should update jenis_prose" do
    patch jenis_prose_url(@jenis_prose), params: { jenis_prose: { created_by: @jenis_prose.created_by, jenisproses_code: @jenis_prose.jenisproses_code, jenisproses_name: @jenis_prose.jenisproses_name, updated_by: @jenis_prose.updated_by } }
    assert_redirected_to jenis_prose_url(@jenis_prose)
  end

  test "should destroy jenis_prose" do
    assert_difference('JenisProse.count', -1) do
      delete jenis_prose_url(@jenis_prose)
    end

    assert_redirected_to jenis_proses_url
  end
end
