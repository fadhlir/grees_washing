require "application_system_test_case"

class TrashRecordingsTest < ApplicationSystemTestCase
  setup do
    @trash_recording = trash_recordings(:one)
  end

  test "visiting the index" do
    visit trash_recordings_url
    assert_selector "h1", text: "Trash Recordings"
  end

  test "creating a Trash recording" do
    visit trash_recordings_url
    click_on "New Trash Recording"

    fill_in "Created by", with: @trash_recording.created_by
    fill_in "Inventory", with: @trash_recording.inventory_id
    check "Is deleted" if @trash_recording.is_deleted
    fill_in "Qty", with: @trash_recording.qty
    fill_in "Status", with: @trash_recording.status
    fill_in "Trash date", with: @trash_recording.trash_date
    click_on "Create Trash recording"

    assert_text "Trash recording was successfully created"
    click_on "Back"
  end

  test "updating a Trash recording" do
    visit trash_recordings_url
    click_on "Edit", match: :first

    fill_in "Created by", with: @trash_recording.created_by
    fill_in "Inventory", with: @trash_recording.inventory_id
    check "Is deleted" if @trash_recording.is_deleted
    fill_in "Qty", with: @trash_recording.qty
    fill_in "Status", with: @trash_recording.status
    fill_in "Trash date", with: @trash_recording.trash_date
    click_on "Update Trash recording"

    assert_text "Trash recording was successfully updated"
    click_on "Back"
  end

  test "destroying a Trash recording" do
    visit trash_recordings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Trash recording was successfully destroyed"
  end
end
