require "application_system_test_case"

class JenisProsesTest < ApplicationSystemTestCase
  setup do
    @jenis_prose = jenis_proses(:one)
  end

  test "visiting the index" do
    visit jenis_proses_url
    assert_selector "h1", text: "Jenis Proses"
  end

  test "creating a Jenis prose" do
    visit jenis_proses_url
    click_on "New Jenis Prose"

    fill_in "Created by", with: @jenis_prose.created_by
    fill_in "Jenisproses code", with: @jenis_prose.jenisproses_code
    fill_in "Jenisproses name", with: @jenis_prose.jenisproses_name
    fill_in "Updated by", with: @jenis_prose.updated_by
    click_on "Create Jenis prose"

    assert_text "Jenis prose was successfully created"
    click_on "Back"
  end

  test "updating a Jenis prose" do
    visit jenis_proses_url
    click_on "Edit", match: :first

    fill_in "Created by", with: @jenis_prose.created_by
    fill_in "Jenisproses code", with: @jenis_prose.jenisproses_code
    fill_in "Jenisproses name", with: @jenis_prose.jenisproses_name
    fill_in "Updated by", with: @jenis_prose.updated_by
    click_on "Update Jenis prose"

    assert_text "Jenis prose was successfully updated"
    click_on "Back"
  end

  test "destroying a Jenis prose" do
    visit jenis_proses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Jenis prose was successfully destroyed"
  end
end
