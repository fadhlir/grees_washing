require "application_system_test_case"

class InventoryHistoriesTest < ApplicationSystemTestCase
  setup do
    @inventory_history = inventory_histories(:one)
  end

  test "visiting the index" do
    visit inventory_histories_url
    assert_selector "h1", text: "Inventory Histories"
  end

  test "creating a Inventory history" do
    visit inventory_histories_url
    click_on "New Inventory History"

    fill_in "Created by", with: @inventory_history.created_by
    fill_in "Date in", with: @inventory_history.date_in
    fill_in "Date out", with: @inventory_history.date_out
    fill_in "Employee name", with: @inventory_history.employee_name
    fill_in "Inventory", with: @inventory_history.inventory_id
    fill_in "Qty in", with: @inventory_history.qty_in
    fill_in "Type", with: @inventory_history.type
    click_on "Create Inventory history"

    assert_text "Inventory history was successfully created"
    click_on "Back"
  end

  test "updating a Inventory history" do
    visit inventory_histories_url
    click_on "Edit", match: :first

    fill_in "Created by", with: @inventory_history.created_by
    fill_in "Date in", with: @inventory_history.date_in
    fill_in "Date out", with: @inventory_history.date_out
    fill_in "Employee name", with: @inventory_history.employee_name
    fill_in "Inventory", with: @inventory_history.inventory_id
    fill_in "Qty in", with: @inventory_history.qty_in
    fill_in "Type", with: @inventory_history.type
    click_on "Update Inventory history"

    assert_text "Inventory history was successfully updated"
    click_on "Back"
  end

  test "destroying a Inventory history" do
    visit inventory_histories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Inventory history was successfully destroyed"
  end
end
