Rails.application.routes.draw do
  resources :jenis_proses
  get 'role_denieds/index', as: "role_denieds"
  get 'invoices/index'
  root 'home#index'
  
  resources :trash_recordings
  resources :customer_achievements, only: [:index] do 
    collection do
      get :transaction_list
    end
  end
  resources :inventory_histories
  resources :customers
  resources :users
  resources :sessions, only: [:new, :create, :destroy]
  resources :orders do 
    collection do 
      get :add_item
      get :get_customers
      get :get_jenis_proses
      get :autocomplete_customer_customer_name
      get :confirm_new
      post :confirm_new
      get :print_count
      get :login_print
      post :login_check_print
    end
  end
  resources :inventories do
    collection do
      get ':id/add', action: :add, as: :add
    end
  end
  resources :order_ins
  resources :order_outs
  resources :invoices do
    collection do
      get ':id/detail', action: :detail
      get :transaction_list
      get :print_count
      get :login_print
      post :login_check_print
    end
  end

  resources :roles do
    collection do 
      post ':id/update_role_user', action: :update_role_user, :as => :update_role_user
    end
  end

  get 'dashboard', to: 'home#index', as: 'dashboard'
  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
end
