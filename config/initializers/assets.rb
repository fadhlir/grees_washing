# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
Rails.application.config.assets.precompile += %w( images/ fonts/ order_count_total.js order.js order_add_item.js customers.js order_update.js home.js filter_date.js invoices.js invoices_new.js inventories.js inventory_histories.js users.js order_show.js invoices_show.js role.js customer_achievements.js trash_recordings.js)
