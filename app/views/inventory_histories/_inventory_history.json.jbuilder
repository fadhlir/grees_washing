json.extract! inventory_history, :id, :inventory_id, :qty_in, :date_in, :date_out, :type, :employee_name, :created_by, :created_at, :updated_at
json.url inventory_history_url(inventory_history, format: :json)
