json.extract! jenis_prose, :id, :jenisproses_code, :jenisproses_name, :created_by, :updated_by, :created_at, :updated_at
json.url jenis_prose_url(jenis_prose, format: :json)
