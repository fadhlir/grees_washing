json.extract! user, :id, :email, :username, :fullname, :role_code, :created_at, :updated_at
json.url user_url(user, format: :json)
