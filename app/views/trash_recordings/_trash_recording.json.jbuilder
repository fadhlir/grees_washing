json.extract! trash_recording, :id, :trash_date, :inventory_id, :created_by, :qty, :status, :is_deleted, :created_at, :updated_at
json.url trash_recording_url(trash_recording, format: :json)
