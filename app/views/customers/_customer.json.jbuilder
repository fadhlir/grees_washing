json.extract! customer, :id, :customer_code, :customer_name, :address, :phone, :created_at, :updated_at
json.url customer_url(customer, format: :json)
