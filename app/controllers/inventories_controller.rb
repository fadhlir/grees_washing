class InventoriesController < ApplicationController
  before_action :set_inventory, only: [:show, :edit, :update, :destroy, :add]
  before_action :current_user
  before_action :authenticate_user
  before_action :check_role_user
  before_action :get_params_header

  # GET /inventories
  # GET /inventories.json
  def index
    @scopes      = Inventory.order("id desc")
    @inventories = @scopes.page(params[:page]).per(params[:per_page])
  end

  # GET /inventories/1
  # GET /inventories/1.json
  def show
  end

  # GET /inventories/new
  def new
    @inventory = Inventory.new

    render layout: false
  end

  # GET /inventories/1/edit
  def edit
    render layout: false
  end

  def add
    render layout: false
  end

  # POST /inventories
  # POST /inventories.json
  def create
    inventory_params[:inventory_histories_attributes] = [{qty: inventory_params[:qty], history_date: Time.now, history_type: 'in', created_by: current_user.id}]
    inventory_params[:user_id] = current_user.id
    @inventory = Inventory.new(inventory_params)

    respond_to do |format|
      if @inventory.save
        format.html { redirect_to inventories_path, notice: 'Inventory was successfully created.' }
        format.json { render :index, status: :created, location: inventories_path }
      else
        format.html { render :new }
        format.json { render json: @inventory.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inventories/1
  # PATCH/PUT /inventories/1.json
  def update
    respond_to do |format|
      if params[:add_stok].blank?
        if inventory_params[:qty].to_i < @inventory.qty
          InventoryHistory.create(inventory_id: @inventory.id, qty: @inventory.qty - inventory_params[:qty].to_i, history_date: Time.now, employee_name: "edited on master", history_type: 'out', created_by: current_user.id)
        elsif inventory_params[:qty].to_i > @inventory.qty
          InventoryHistory.create(inventory_id: @inventory.id, qty: inventory_params[:qty].to_i - @inventory.qty, history_date: Time.now, employee_name: "edited on master", history_type: 'in', created_by: current_user.id)
        end
      else
        inventory_params[:qty] = inventory_params[:qty].to_i + @inventory.qty
        InventoryHistory.create(inventory_id: @inventory.id, qty: inventory_params[:qty].to_i - @inventory.qty, history_date: Time.now, employee_name: "edited on master", history_type: 'in', created_by: current_user.id)
      end
      if @inventory.update(inventory_params)
        format.html { redirect_to inventories_path, notice: 'Inventory was successfully updated.' }
        format.json { render :index, status: :ok, location: inventories_path }
      else
        format.html { redirect_to inventories_path, notice: 'Gagal update inventory.' }
        format.json { render json: @inventory.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inventories/1
  # DELETE /inventories/1.json
  def destroy
    @inventory.destroy
    respond_to do |format|
      format.html { redirect_to inventories_url, notice: 'Inventory was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def get_params_header
      @start_date = params[:start_date].present? ? params[:start_date] : Time.now - 7.days
      @end_date = params[:end_date].present? ? params[:end_date] : Time.now
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_inventory
      @inventory = Inventory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inventory_params
      params.require("inventory").permit!
    end
end
