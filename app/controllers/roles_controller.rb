class RolesController < ApplicationController
  before_action :current_user
  before_action :authenticate_user
  before_action :check_role_user
  before_action :find_role, only: [:edit, :show, :destroy]
  before_action :get_params_header
  before_action :get_role_lists, only: [:index]
  before_action :get_params_input, only: [:new]

  def index
  end

  def new
    @role = Role.new
    render layout: false
  end

  def edit
    render layout: false
  end

  def create
    role = Role.new(role_params)
    if role.save
      redirect_to roles_path, notice: 'Role was successfully created.'
    else
      redirect_to roles_path, notice: 'Role was failed created.'
    end
  end

  def update_role_user
    @role = Role.find(params[:id])
    @role.role_users.delete_all
    if @role.update(role_params)
      redirect_to roles_path, notice: 'Role was successfully created.'
    else
      redirect_to roles_path, notice: 'Role was failed created.'
    end
  end

  def destroy
    @role.destroy
    @role.role_users.delete_all
    respond_to do |format|
      format.html { redirect_to roles_url, notice: 'Role was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def get_role_lists
      @roles = Role.order("id desc").page(params[:page]).per(params[:per_page])
    end

    def get_params_header
      @start_date = params[:start_date].present? ? params[:start_date] : Time.now - 7.days
      @end_date = params[:end_date].present? ? params[:end_date] : Time.now
      @hide_filter = true
    end

    def get_params_input
      # @menus = Menu.order(:id).map{|menu| [menu.menu_name, menu.id]}
      @menus = Menu.order(:menu_name)
    end

    def find_role
      @role = Role.find(params[:id])
    end

    def role_params
      params.require(:role).permit!
    end
end
