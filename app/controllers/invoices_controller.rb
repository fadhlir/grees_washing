class InvoicesController < ApplicationController
  before_action :current_user
  before_action :authenticate_user
  before_action :check_role_user
  before_action :get_params_header
  before_action :get_list_invoices, only: [:index]
  before_action :find_invoice, only: [:show, :detail, :edit, :update, :destroy]
  before_action :get_header_params, only: [:new]
  def index
  end

  def show
    render layout: false
  end

  def detail
    @hide_filter = true    
  end

  def edit
    render layout: false
  end

  def update
    respond_to do |format|
      if params[:post][:status_invoice] == "done"
        InvoiceMaster.make_invoice_done(@invoice.id)
        format.js {redirect_to invoices_path}
      end
    end
  end

  def new
    @invoice = InvoiceMaster.new
  end

  def transaction_list
    id_customer     = params[:id_customer]
    @customer       = Customer.find(id_customer)
    @order_invoices = OrderInvoice.where(customer_id: id_customer, status_payment: "debt")

    render layout: false
  end

  def create
    if params_post[:invoice_details_attributes].blank?
      flash[:notice] = "Invoice gagal dibuat, pilih minimal satu transaksi customer!"
      redirect_to new_invoice_path
    else
      params_save = InvoiceMaster.generate_invoice(params_post)
      invoice     = InvoiceMaster.new(params_save)
      if invoice.save
        flash[:notice] = "Invoice barhasil dibuat!"
        redirect_to invoice_path(invoice.id)+"/detail"
      else
        flash[:notice] = "Invoice gagal dibuat, cobalah beberapa saat lagi atau hubungi admin!"
        redirect_to new_invoice_path
      end
    end
  end

  def destroy
    invoice_details = InvoiceDetail.where(invoice_master_id: @invoice.id)
    if invoice_details.destroy_all
      if @invoice.destroy
        respond_to do |format|
          format.html { redirect_to invoices_url, notice: 'Invoice berhasil dihapus' }
          format.json { head :no_content }
        end
      else
        respond_to do |format|
          format.html { redirect_to invoices_url, notice: 'Invoice gagal dihapus' }
          format.json { head :no_content }
        end
      end
    end
  end

  def print_count
    find_invoice
    if @invoice.update(params_post)
      render :html => "berhasil"
    else
      render :html => "gagal"
    end
  end

  def login_print
    render layout: false
  end

  def login_check_print
    user = User.find_by_email(params[:email])
    @login = (user && user.authenticate(params[:password])) ? true : false
    if @login
      menus_role = Menu.where("id in (?)", user.role.role_users.pluck(:menu_id)).pluck(:menu_name)
      @login = menus_role.include?("Print Twice") ? true : false
    end
  end

  private

  def get_params_header
    @start_date = params[:start_date].present? ? params[:start_date] : Time.now - 7.days
    @end_date = params[:end_date].present? ? params[:end_date] : Time.now
  end

  def get_list_invoices
    @invoices = InvoiceMaster.search(params[:search], params[:start_date], params[:end_date]).page(params[:page]).per(params[:per_page]).order("invoice_masters.id desc")
  end

  def find_invoice
    @invoice = InvoiceMaster.find(params[:id])
  end

  def get_header_params
    @customers = Customer.order(:customer_name)
  end

  def params_post
    params.require("post").permit!
  end
end
