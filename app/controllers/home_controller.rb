class HomeController < ApplicationController  
  before_action :current_user
  before_action :authenticate_user
  before_action :check_role_user
  before_action :get_params_header, only: [:index]

  def index
    if params[:start_date].present?
      respond_to do |format|
        format.js
      end
    end
  end

  private

  def get_params_header
    @start_date = params[:start_date].present? ? params[:start_date] : Time.now - 7.days
    @end_date = params[:end_date].present? ? params[:end_date] : Time.now
    @order_invoice = OrderInvoice.search(nil, @start_date, @end_date, nil)
    @total_invoice = @order_invoice.count

    @order_item = @order_invoice.joins(:order_item_details).count
    @income = @order_invoice.pluck(:cash).sum
    @debt = @order_invoice.pluck(:left_payment).sum

    @chart_invoice = Array.new
    date_range = (@start_date.to_date..@end_date.to_date).map{ |date| date.strftime("%Y-%m-%d") }
    date_range.each  do |f| 
      data = [f.to_date.strftime("%b %d"), @order_invoice.where("DATE(created_at) = ?", f.to_date).count]
      @chart_invoice.push(data)
    end

  end

end
