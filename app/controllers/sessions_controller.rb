class SessionsController < ApplicationController
  layout "login"

  def new
  end

  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id]   = user.id
      flash[:notice]  = "Proses sign in berhasil!"
      redirect_to root_url
    else
      flash.now[:alert] = "Email or password id invalid"
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: "Logged out!"
  end
end
