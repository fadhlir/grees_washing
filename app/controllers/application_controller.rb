class ApplicationController < ActionController::Base
  helper_method :current_user

  def current_user
    if session[:user_id]
      @current_user ||= User.find(session[:user_id])
    else
      @current_user = nil
    end
  end

  def authenticate_user
    if @current_user.blank?
      redirect_to login_path
    end
  end

  def check_role_user
    menu = nil
    case params[:controller]
    when "home"
      menu = "Dashboard"
    when "customers"
      menu = "Master Customer"
    when "inventories"
      menu = "Master Inventory"
    when "inventory_histories"
      menu = "History Inventory"
    when "invoices"
      menu = "List Invoice"
    when "order_ins"
      menu = "Barang In Progress"
    when "order_outs"
      menu = "Barang Completed"
    when "orders"
      menu = "Create Transaction"
    when "role_denieds"
      menu = "Role Denieds"
    when "roles"
      menu = "Role User"
    when "users"
      menu = "Master User"
    when "jenis_proses"
      menu = "Master Jenis Proses"
    end

    @roles_menu_id = @current_user.role.role_users.pluck(:menu_id)
    menu_roles = Menu.where("id in (?)", @roles_menu_id).pluck(:menu_name)
    menu_roles.push("Role Denieds")
    if !menu_roles.include?(menu)
      redirect_to role_denieds_path
    end
  end
end
