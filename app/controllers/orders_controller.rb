class OrdersController < ApplicationController
  before_action :current_user
  before_action :authenticate_user
  before_action :check_role_user
  before_action :get_params_header
  before_action :get_list_orders, only: [:index]
  before_action :find_order, only: [:show, :edit, :update, :destroy]

  def index
  end

  def show
    @order_items    = @order.order_item_details    
    render layout: false
  end

  def new
    @hide_filter = true
    @order = OrderInvoice.new
  end

  def create
    params_invoice                  = Hash.new
    params_invoice[:order_invoice]  = order_invoice_params
    params_invoice[:order_invoice][:created_by] = current_user.id
    order = OrderInvoice.new(OrderInvoice.get_total_order(params_invoice))

    if order.save
      @order = order
      flash[:notice] = "Transaksi berhasil di simpan!"
    else
      @order = order
      flash[:notice] = "Transaksi Gagal di simpan!"
    end
  end

  def edit
    render layout: false
  end

  def update
    respond_to do |format|
      if @order.update(post_params)
        OrderInvoice.update_order_invoice(@order.id)
        flash[:notice] = "Transaksi #{@order.invoice_number}, customer #{@order.customer_name} berhasil di update!"
        format.js {redirect_to orders_path}
        # format.html { redirect_to @user, notice: 'User was successfully updated.' }
        # format.json { render :show, status: :ok, location: @user }
      else
        render :html => "gagal"
      end
    end
  end

  def print_count
    find_order
    if @order.update(post_params)
      render :html => "berhasil"
    else
      render :html => "gagal"
    end
  end

  def login_print
    render layout: false
  end

  def login_check_print
    user = User.find_by_email(params[:email])
    @login = (user && user.authenticate(params[:password])) ? true : false
    if @login
      menus_role = Menu.where("id in (?)", user.role.role_users.pluck(:menu_id)).pluck(:menu_name)
      @login = menus_role.include?("Print Twice") ? true : false
    end
  end

  def destroy
    order_item_details = OrderItemDetail.where(invoice_number: @order.invoice_number)
    if order_item_details.destroy_all
      if @order.destroy
        respond_to do |format|
          format.html { redirect_to orders_url, notice: 'Transaksi berhasil dihapus' }
          format.json { head :no_content }
        end
      else
        respond_to do |format|
          format.html { redirect_to orders_url, notice: 'Transaksi gagal dihapus' }
          format.json { head :no_content }
        end
      end
    end
  end

  def add_item
    @class_order_item = params[:class_order_item].present? ? params[:class_order_item] : "order-item-1"
    @index            = params[:index_array].present?      ? params[:index_array]      : 1
    render layout: false
  end

  def get_customers
    @customers = Customer.search(params[:term])
    render json: @customers.map{|v| {id: v.id, label: v.code_name, value: v.customer_name}}
  end

  def get_jenis_proses
    @jenis_proses = JenisProse.where("lower(jenisproses_code) like ? or lower(jenisproses_name) like ?", "%#{params[:term]}%", "%#{params[:term]}%")
    render json: @jenis_proses.map{|v| {id: v.id, label: v.code_name, value: v.jenisproses_name}}
  end

  def confirm_new
    respond_to do |format|
      @hide_filter = true
      if params[:post].present?
        params_post = Hash.new
        params_post[:order_invoice] = params[:post]
        params_post[:order_invoice][:created_by] = current_user.id
        @params = OrderInvoice.get_total_order(params_post)
        bahagia = OrderInvoice.new(post_params)
        if bahagia.valid?
          format.html {}
        else
          format.html {redirect_to new_order_path, notice: bahagia.errors}
        end
      else
        format.html {redirect_to new_order_path}
      end
    end
  end

  private

  def get_params_header
    @start_date = params[:start_date].present? ? params[:start_date] : Time.now - 7.days
    @end_date = params[:end_date].present? ? params[:end_date] : Time.now
  end

  def get_list_orders
    @orders = OrderInvoice.search(params[:search], @start_date, @end_date, params[:status])
    if params[:status] == "done"
      @orders = @orders.where("order_invoices.status_payment = ?", "done")
    end
    @orders = @orders.order(created_at: :desc).page(params[:page]).per(params[:per_page])
  end

  def find_order
    @order = OrderInvoice.find(params[:id])    
  end

  def order_invoice_params    
    params.require("order_invoice").permit!
  end

  def post_params
    params.require("post").permit!
  end
end
