class JenisProsesController < ApplicationController
  before_action :current_user
  before_action :check_role_user
  before_action :authenticate_user
  before_action :get_params_header
  before_action :get_jenis_proses_lists, only: [:index]
  before_action :set_jenis_prose, only: [:show, :edit, :update, :destroy]

  # GET /jenis_proses
  # GET /jenis_proses.json
  def index
    @hide_filter = true
  end

  # GET /jenis_proses/1
  # GET /jenis_proses/1.json
  def show
  end

  # GET /jenis_proses/new
  def new
    @jenis_prose = JenisProse.new
    render layout: false
  end

  # GET /jenis_proses/1/edit
  def edit
    render layout: false
  end

  # POST /jenis_proses
  # POST /jenis_proses.json
  def create
    jenis_proses = jenis_prose_params
    jenis_proses[:created_by] = current_user.id
    jenis_proses[:updated_by] = current_user.id
    @jenis_prose = JenisProse.new(jenis_proses)

    if params[:from].eql?("orders")
      respond_to do |format|
        if @jenis_prose.save
          flash[:notice] = "Tambah Data Jenis Proses Berhasil"
          format.js { render layout: false}
        else
          flash[:notice] = "Tambah Data Jenis Proses Gagal"
          format.js { render layout: false}
        end
      end
    else
      respond_to do |format|
        if @jenis_prose.save
          format.html { redirect_to jenis_proses_path, notice: 'Jenis proses Berhasil dibuat' }
          format.json { render :show, status: :created, location: @jenis_prose }
        else
          format.html { redirect_to jenis_proses_path, notice: @jenis_prose.errors.messages.first.first.to_s + " " + @jenis_prose.errors.messages.first.last.last.to_s }
          format.json { render json: @jenis_prose.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /jenis_proses/1
  # PATCH/PUT /jenis_proses/1.json
  def update
    jenis_proses = jenis_prose_params
    jenis_proses[:updated_by] = current_user.id
    respond_to do |format|
      if @jenis_prose.update(jenis_proses)
        format.html { redirect_to jenis_proses_path, notice: 'Jenis prose Berhasil di update.' }
        format.json { render :show, status: :ok, location: @jenis_prose }
      else
        format.html { redirect_to jenis_proses_path, notice: @jenis_prose.errors.messages.first.first.to_s + " " + @jenis_prose.errors.messages.first.last.last.to_s }
        format.json { render json: @jenis_prose.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jenis_proses/1
  # DELETE /jenis_proses/1.json
  def destroy
    @jenis_prose.destroy
    respond_to do |format|
      format.html { redirect_to jenis_proses_url, notice: 'Jenis prose was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def get_jenis_proses_lists
      @scopes       = JenisProse.where("lower(jenisproses_code) like ? or lower(jenisproses_name) like ?", "%#{params[:search].try(:downcase)}%", "%#{params[:search].try(:downcase)}%").order("id desc")
      @jenis_proses = @scopes.page(params[:page]).per(params[:per_page])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_jenis_prose
      @jenis_prose = JenisProse.find(params[:id])
    end

    def get_params_header
      @start_date = params[:start_date].present? ? params[:start_date] : Time.now - 7.days
      @end_date = params[:end_date].present? ? params[:end_date] : Time.now
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def jenis_prose_params
      params.require(:jenis_prose).permit(:jenisproses_code, :jenisproses_name, :created_by, :updated_by)
    end
end
