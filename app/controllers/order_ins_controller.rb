class OrderInsController < ApplicationController
  before_action :current_user
  before_action :get_params_header, only: [:index]
  before_action :get_list_order_items, only: [:index]
  before_action :authenticate_user
  before_action :check_role_user

  def index; end

  private

  def get_list_order_items
    @orders      = OrderItemDetail.search('progress', params[:search], params[:start_date], params[:end_date], params[:jenis_proses])
    @order_items = @orders.page(params[:page]).per(params[:per_page])
  end

  def get_params_header
    @start_date = params[:start_date].present? ? params[:start_date] : Date.today - 7.days
    @end_date = params[:end_date].present? ? params[:end_date] : Date.today
    @jenis_proses = JenisProse.order(:jenisproses_name).map{|v| [v.code_name, v.id]}
  end
end
