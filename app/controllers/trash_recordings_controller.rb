class TrashRecordingsController < ApplicationController
  before_action :current_user
  before_action :authenticate_user
  before_action :get_params_header, only: [:index, :show]
  before_action :get_parent, only: [:new, :edit]
  before_action :set_trash_recording, only: [:show, :edit, :update, :destroy]

  # GET /trash_recordings
  # GET /trash_recordings.json
  def index;end

  # GET /trash_recordings/1
  # GET /trash_recordings/1.json
  def show
  end

  # GET /trash_recordings/new
  def new
    @trash_recording = TrashRecording.new
    render layout: false
  end

  # GET /trash_recordings/1/edit
  def edit;end

  # POST /trash_recordings
  # POST /trash_recordings.json
  def create
    params[:trash_recording][:created_by] = @current_user.id
    @trash_recording = TrashRecording.new(trash_recording_params)

    respond_to do |format|
      if @trash_recording.save
        format.html { redirect_to @trash_recording, notice: 'Trash recording was successfully created.' }
        format.json { render :show, status: :created, location: @trash_recording }
      else
        format.html { render :new }
        format.json { render json: @trash_recording.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trash_recordings/1
  # PATCH/PUT /trash_recordings/1.json
  def update
    respond_to do |format|
      if @trash_recording.update(trash_recording_params)
        format.html { redirect_to @trash_recording, notice: 'Trash recording was successfully updated.' }
        format.json { render :show, status: :ok, location: @trash_recording }
      else
        format.html { render :edit }
        format.json { render json: @trash_recording.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trash_recordings/1
  # DELETE /trash_recordings/1.json
  def destroy
    @trash_recording.destroy
    respond_to do |format|
      format.html { redirect_to trash_recordings_url, notice: 'Trash recording was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def get_parent
      @inventories = Inventory.order(:name)
    end

    def get_params_header
      @start_date = params[:start_date].present? ? params[:start_date] : Time.now - 7.days
      @end_date = params[:end_date].present? ? params[:end_date] : Time.now
      @trash_recordings = TrashRecording.joins(:inventory).where("DATE(trash_recordings.created_at) between ? and ? and inventories.name like ?", @start_date, @end_date, "%#{params[:search]}%").order("created_at desc")
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_trash_recording
      @trash_recording = TrashRecording.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trash_recording_params
      params.require(:trash_recording).permit(:trash_date, :inventory_id, :created_by, :qty, :status, :is_deleted)
    end
end
