class InventoryHistoriesController < ApplicationController
  before_action :set_inventory_history, only: [:show, :edit, :update, :destroy]  
  before_action :current_user
  before_action :authenticate_user
  before_action :check_role_user
  before_action :get_params_header
  before_action :get_list_inventory_histories, only: [:index]

  # GET /inventory_histories
  # GET /inventory_histories.json
  def index
  end

  # GET /inventory_histories/1
  # GET /inventory_histories/1.json
  def show
  end

  # GET /inventory_histories/new
  def new
    @inventory_history = InventoryHistory.new
    @inventory = Inventory.order(:name)
    render layout: false
  end

  # GET /inventory_histories/1/edit
  def edit
  end

  # POST /inventory_histories
  # POST /inventory_histories.json
  def create
    @inventory_history = InventoryHistory.new(inventory_history_params)
    inventory = Inventory.find(@inventory_history.inventory_id)
    respond_to do |format|
      if inventory.qty < @inventory_history.qty
        format.html { redirect_to inventory_histories_path(type: "out"), notice: 'Stok barang inventory tidak cukup, mohon update data stok di master inventory terlebih dahul.' }
        format.json { render json: @inventory_history.errors, status: :unprocessable_entity }
      else
        if @inventory_history.save
          qty_inventory = inventory.qty
          inventory.update_attributes(:qty => qty_inventory - @inventory_history.qty)
          format.html { redirect_to inventory_histories_path, notice: 'Inventory history was successfully created.' }
          format.json { render :incdex, status: :created, location: inventory_histories_path(type: "out") }
        else
          format.html { render :new }
          format.json { render json: @inventory_history.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /inventory_histories/1
  # PATCH/PUT /inventory_histories/1.json
  def update
    respond_to do |format|
      if @inventory_history.update(inventory_history_params)
        format.html { redirect_to @inventory_history, notice: 'Inventory history was successfully updated.' }
        format.json { render :show, status: :ok, location: @inventory_history }
      else
        format.html { render :edit }
        format.json { render json: @inventory_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inventory_histories/1
  # DELETE /inventory_histories/1.json
  def destroy
    @inventory_history.destroy
    respond_to do |format|
      format.html { redirect_to inventory_histories_url, notice: 'Inventory history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def get_list_inventory_histories
      @inventory_histories = InventoryHistory.search(params[:search], params[:start_date], params[:end_date], params[:type]).page(params[:page]).per(params[:per_page])
    end

    def get_params_header
      @start_date = params[:start_date].present? ? params[:start_date] : Time.now - 7.days
      @end_date = params[:end_date].present? ? params[:end_date] : Time.now
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_inventory_history
      @inventory_history = InventoryHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inventory_history_params
      params.require(:inventory_history).permit!
    end
end
