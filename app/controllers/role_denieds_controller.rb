class RoleDeniedsController < ApplicationController
  before_action :current_user
  before_action :authenticate_user
  before_action :get_params_header

  def index
  end

  private

    def get_params_header
      @start_date = params[:start_date].present? ? params[:start_date] : Time.now - 7.days
      @end_date = params[:end_date].present? ? params[:end_date] : Time.now
      @hide_filter = true
    end
end
