class CustomersController < ApplicationController
  before_action :current_user
  before_action :authenticate_user
  before_action :check_role_user
  before_action :get_params_header
  before_action :set_customer, only: [:show, :edit, :update, :destroy]
  before_action :get_customers_list, only: [:index]

  # GET /customers
  # GET /customers.json
  def index
  end

  # GET /customers/1
  # GET /customers/1.json
  def show
    render layout: false
  end

  # GET /customers/new
  def new
    @customer = Customer.new
    render layout: false
  end

  # GET /customers/1/edit
  def edit
    render layout: false
  end

  # POST /customers
  # POST /customers.json
  def create
    respond_to do |format|
      @customer = Customer.new(customer_params)
      if @customer.save
        get_customers_list
        flash[:notice] = "Tambah Data Customer Berhasil"
        format.js { render layout: false}
      else
        format.js { render layout: false }
      end
    end
  end

  # PATCH/PUT /customers/1
  # PATCH/PUT /customers/1.json
  def update
    respond_to do |format|
      if @customer.update(customer_params)
        get_customers_list
        flash[:notice] = "Data Customer Berhasil di update"
        format.js { render layout: false}
      else
        format.html { render :edit }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customers/1
  # DELETE /customers/1.json
  def destroy
    @customer.destroy
    respond_to do |format|
      format.html { redirect_to customers_url, notice: 'Customer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def get_params_header
      @start_date = params[:start_date].present? ? params[:start_date] : Time.now - 7.days
      @end_date = params[:end_date].present? ? params[:end_date] : Time.now
    end
    
    def get_customers_list
      @customers = Customer.all.order("id desc")
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      @customer = Customer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customer_params
      params.require(:customer).permit(:customer_code, :customer_name, :address, :phone)
    end
end
