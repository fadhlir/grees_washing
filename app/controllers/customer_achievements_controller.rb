class CustomerAchievementsController < ApplicationController
  before_action :current_user
  before_action :authenticate_user
  before_action :get_parent_params, only: [:index]
  before_action :get_params, only: [:transaction_list, :index]

  def index
    @hide_filter = true
  end

  def transaction_list
    @orders                 = OrderInvoice.search(@customer.try(:customer_name), @start_date, @end_date, nil).order(created_at: :desc)
    @total_all_item         = @orders.pluck(:total_item).sum
    @total_all_qty_item     = @orders.pluck(:total_qty_item).sum
    @total_all_cash         = @orders.pluck(:cash).sum
    @total_all_left_payment = @orders.pluck(:left_payment).sum
    @total_all_invoice      = @orders.pluck(:total_invoice).sum
  end

  private
    def get_parent_params
      @customers = Customer.order(:customer_name)
    end

    def get_params
      @customer = params[:customer_id].present? ? Customer.find_by_id(params[:customer_id]) : nil
      @start_date = params[:start_date].present? ? params[:start_date].to_date : Date.today.beginning_of_week
      @end_date = params[:end_date].present? ? params[:end_date].to_date : Date.today
    end
end
