module OrderHelper
  def get_order_item(invoice_number)
    item = OrderItemDetail.select("product_name").where(invoice_number: invoice_number)
    order_item = ""
    item.each_with_index do |item_name, i|
      seperator = ","
      if i+1 == item.count
        seperator = ""
      end

      order_item += item_name.product_name.to_s + seperator + " "
    end

    return order_item
  end
end
