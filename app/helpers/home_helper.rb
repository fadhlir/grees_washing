module HomeHelper
  def number_to_rupiah(number)
    number_to_currency(number,:unit=>'').to_s.split('.').first
  end
end
