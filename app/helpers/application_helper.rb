module ApplicationHelper
  def list_role_menu 
    @roles_menu_id = @current_user.role.role_users.pluck(:menu_id)
    menu_roles = Menu.where("id in (?)", @roles_menu_id).pluck(:menu_name)

    return menu_roles
  end
end
