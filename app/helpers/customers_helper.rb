module CustomersHelper
  def get_customer_address(customer_id)
    Customer.find_by_id(customer_id).address
  end
end
