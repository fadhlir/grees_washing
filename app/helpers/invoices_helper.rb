module InvoicesHelper
  def find_invoice_detail(invoice_master_id)
    scope = InvoiceMaster.joins("LEFT JOIN invoice_details on invoice_details.invoice_master_id = invoice_masters.id").joins("LEFT JOIN order_invoices on order_invoices.id = invoice_details.order_invoice_id").joins("LEFT JOIN order_item_details on order_item_details.invoice_number = order_invoices.invoice_number").select("invoice_masters.invoice_master_code, invoice_masters.total_invoice, invoice_masters.status_invoice, order_invoices.invoice_number as order_number, order_invoices.total_item as order_total_item, order_invoices.total_qty_item, order_invoices.total_invoice as order_invoice, order_invoices.cash, order_invoices.left_payment, order_invoices.created_at as order_date, order_item_details.qty, order_item_details.product_name, order_item_details.process_name, order_item_details.status_order_item, order_item_details.unit_price, order_item_details.total_price").where("invoice_masters.id = ?", invoice_master_id).order("order_invoices.invoice_number")
    return scope
  end

end
