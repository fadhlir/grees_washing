$(document).ready ->
  $(".launch_modal").click ->
    data_link   = $(this).attr("data-link")
    data_title  = $(this).attr("data-title")
    console.log("Get Ajax "+data_link)

    $.ajax data_link,
      type: 'GET'
      dataType: 'html'
      error: (jqXHR, textStatus, errorThrown) ->
        $('.modal-body').html("AJAX Error: #{textStatus}")
      success: (data, textStatus, jqXHR) ->
        $('#modal_show .modal-dialog').html(data)
        $('.modal-title').html(data_title)

  $(".trigger-transaksi").click ->
    $(".show-list-transaksi-master").toggle()