$(".print").click ->
  data_link   = $(this).attr("data-link")
  id          = $(this).attr("data-orderid")
  print_count = $(this).attr("data-printcount")
  
  if print_count > 1
    $.ajax '/orders/login_print',
    type: 'GET'
    dataType: 'html'
    error: (jqXHR, textStatus, errorThrown) ->
      console.log("Gagal show login" + textStatus)
    success: (data, textStatus, jqXHR) ->
      $(".main-content").append(data)
      $("#modal_login_print").modal("show")
      console.log("Show login print bos")
  else
    window.print()
    $.ajax data_link,
    type: 'GET'
    dataType: 'html'
    data: {post: {print_count: print_count}, id: id}
    error: (jqXHR, textStatus, errorThrown) ->
      console.log("Gagal " + textStatus)
    success: (data, textStatus, jqXHR) ->
      $(".print").attr("data-printcount", parseInt(print_count) + 1)
      console.log("Berhasil " + data)