$(document).ready ->
  $(".filter-date-today").click ->
    $(".main-content").html("<img src='/assets/loading.gif' style='display:flex; margin:auto; width:60px; height:60px;'>")
    $('.time-filter-tag').html("Today: ")
    $('.btn-today').addClass("btn-primary")
    $('.btn-month').removeClass("btn-primary")
    $('.btn-year').removeClass("btn-primary")

  $(".filter-date-month").click ->
    $(".main-content").html("<img src='/assets/loading.gif' style='display:flex; margin:auto; width:60px; height:60px;'>")
    $('.btn-today').removeClass("btn-primary")
    $('.btn-month').addClass("btn-primary")
    $('.btn-year').removeClass("btn-primary")
    $('.time-filter-tag').html("This Month: ")

  $(".filter-date-year").click ->
    $(".main-content").html("<img src='/assets/loading.gif' style='display:flex; margin:auto; width:60px; height:60px;'>")
    $('.btn-today').removeClass("btn-primary")
    $('.btn-month').removeClass("btn-primary")
    $('.btn-year').addClass("btn-primary")
    $('.time-filter-tag').html("This Month: ")

  dateFormat = "dd-mm-yy"

  start_date = $("#filter-start-date").datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    dateFormat: "dd-mm-yy",
    numberOfMonths: 1
  }).on 'change', ->
    end_date.datepicker( "option", "minDate", getDate( this ) )
    $("#start_date").val($(this).val())

  end_date = $("#filter-end-date").datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    dateFormat: "dd-mm-yy",
    numberOfMonths: 1
  }).on 'change', ->
    start_date.datepicker( "option", "maxDate", getDate( this ) )
    $("#end_date").val($(this).val())

  getDate = (element) ->
    try 
      date = $.datepicker.parseDate(dateFormat, element.value)
    catch error 
      date = null
    return date

  $('.date-dropdown').on 'click', ->
    $(".dropdown-menu").toggleClass('open')

  $(document).on 'click', '.dropdown-menu', (e) ->
    e.stopPropagation()
  $(document).on 'click', '.ui-icon', (e) ->
    e.stopPropagation()
  $('.btn-close-date').click ->
    $('button.dropdown-toggle').dropdown('toggle')  
