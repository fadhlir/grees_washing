$('.add-item').click ->
  array_index = $(this).attr("data-array")
  add_item(array_index)

add_item = (array_index) ->
  index        = array_index
  data_link    = "/orders/add_item"
  class_order  = "order-item-" + index
  console.log("index = "+index+"; class = "+class_order)

  $.ajax data_link,
    type: 'GET'
    data: {class_order_item: class_order, index_array: index}
    dataType: 'html'
    error: (jqXHR, textStatus, errorThrown) ->
      console.log(errorThrown)
    success: (data, textStatus, jqXHR) ->
      $('.order-item').append(data)
      $('.add-item').attr("data-array", parseInt(index) + 1)

$("input[data-autocomplete]").each ->
    url = $(this).data('autocomplete')
    target = $(this).data('target')
    $(this).on 'input', ->
      $("#"+target).val("")
    $(this).autocomplete
      source: url
      classes: "ui-autocomplete": "highlight"
      select: (event, ui) ->
        $("#"+target).val(ui.item.id)
        $(this).css({"border-color": "#28a745"})
        $('.invalid-customer').css({"display": "none"})
