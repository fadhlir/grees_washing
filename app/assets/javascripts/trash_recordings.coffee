$(".launch-modal-trash-record").click ->
  data_link   = $(this).attr("data-link")
  data_title  = $(this).attr("data-title")

  $.ajax data_link,
    type: "GET",
    data: {title: data_title},
    dataType: 'html'
    error: (e) ->
      $('.modal-body').html("Error :" + e)
    success: (data) ->
      $('.modal-title').html(data_title)
      $('.modal-body').html(data)