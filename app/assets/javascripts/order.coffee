$(document).ready ->
  $(".launch_modal").click ->
    data_link   = $(this).attr("data-link")
    data_title  = $(this).attr("data-title")

    $.ajax data_link,
      type: 'GET'
      dataType: 'html'
      error: (jqXHR, textStatus, errorThrown) ->
        $('.modal-body').html("AJAX Error: #{textStatus}")
      success: (data, textStatus, jqXHR) ->
        $('#modal_show .modal-dialog').html(data)
        $('#modal_show .modal-title').html(data_title)

  $(".field-price").on "input", ->
    target_total = $(this).attr('data-target-total')
    target_qty   = $(this).attr('data-target-qty')
    qty          = $("#"+target_qty).val()

    $("#"+target_total).val(qty * $(this).val())