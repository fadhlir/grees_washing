$(document).ready ->
  $(".launch-modal-customer").click ->
    data_link   = $(this).attr("data-link")
    data_title  = $(this).attr("data-title")
    console.log("Get Ajax "+data_link)

    $.ajax data_link,
      type: 'GET'
      data: {title: data_title, from: "orders"}
      dataType: 'html'
      error: (jqXHR, textStatus, errorThrown) ->
        $('.modal-body').html("AJAX Error: #{textStatus} #{errorThrown}")
      success: (data, textStatus, jqXHR) ->
        if data_title == "Add New Jenis Proses"
          $('.modal-footer').html("")
          $('.modal-title').html(data_title)
          $('.modal-body').html(data)
        else
          $('.modal-content').html(data)
        end
        