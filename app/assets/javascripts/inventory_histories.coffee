$(document).ready ->
  $(".launch-modal-inventory").click ->
    data_link   = $(this).attr("data-link")
    data_title  = $(this).attr("data-title")
    console.log("Get Ajax "+data_link)

    $.ajax data_link,
      type: 'GET'
      dataType: 'html'
      error: (jqXHR, textStatus, errorThrown) ->
        $('.modal-body').html("AJAX Error: #{textStatus}")
      success: (data, textStatus, jqXHR) ->
        $('.modal-title').html(data_title)
        $('.modal-body').html(data)
        if data_link.split("/")[data_link.split("/").length - 1]  == "edit"
          $('.modal-footer').html("")
        else
          $('.modal-footer').html("<button type='button' class='btn btn-secondary btn-custom' data-dismiss='modal'>Tutup</button>")