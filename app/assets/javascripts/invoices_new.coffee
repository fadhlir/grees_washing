$(document).ready ->
  $("#post_customer_id").on 'change', ->
    id = $(this).val()
    $.ajax '/invoices/transaction_list?id_customer='+id,
      type: "GET"
      error: (error, textStatus, errorThrown) ->
        console.log(errorThrown)
        console.log(textStatus)
      success: (data, textStatus, jqXhr) ->
        console.log(textStatus)
        $('.list-transactions-select').html(data)

  $("#check_all").on 'change', ->
    $('.order-invoice-check').prop('checked', this.checked)

  $(".btn-create-invoice").on 'click', ->
    # $(this).prop('disabled', true)