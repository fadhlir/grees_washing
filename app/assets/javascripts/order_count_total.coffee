$(".field-price").on "input", ->
  target_total = $(this).attr('data-target-total')
  target_qty   = $(this).attr('data-target-qty')
  qty          = $("#"+target_qty).val()

  $("#"+target_total).val(qty * $(this).val())

$("#post_customer_name").on "focus", ->
  count_all_total()


$("#post_customer_name").on "focusout", ->
  if $("#post_customer_id").val() == ""
    $(this).css({"border-color": "#dc3545"})
    $('.invalid-customer').css({"display": "block"})
  else
    $(this).css({"border-color": "#28a745"})
    $('.invalid-customer').css({"display": "none"})

count_all_total = () ->
  total = 0
  $('.field-total').each ->
    total = total + parseInt($(this).val())
  $("#post_total_invoice").val(total)

$('#post_cash').on "input", ->
  count_change()

count_change = () ->
  total_bayar   = parseInt($('#post_total_invoice').val())
  jumlah_bayar  = parseInt($('#post_cash').val())

  if total_bayar < jumlah_bayar
    change = jumlah_bayar - total_bayar
    $(".description-pay").html("Kembalian")
  else if total_bayar == jumlah_bayar
    change = 0
  else
    $(".description-pay").html("Sisa Bayar")
    change = total_bayar - jumlah_bayar

  $("#change").html(change)

$('.close-item').click ->
  class_order_item = $(this).attr("data-orderitem")
  $("."+class_order_item).remove()