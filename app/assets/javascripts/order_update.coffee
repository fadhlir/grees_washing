$('#post_status_payment').on 'change', ->
  if $(this).val() == "done"
    $('#post_cash').val($('#total_harga').html())
    hitung_sisa_bayar()

$('#post_cash').on 'input', ->
  hitung_sisa_bayar()

$('#post_cash').on 'focus', ->
  hitung_sisa_bayar()

hitung_sisa_bayar = () ->
  cash        = parseInt($('#post_cash').val())
  total_harga = parseInt($('#total_harga').html())
  
  $('#sisa_bayar').html(total_harga - cash)
  $('#post_left_payment').val(total_harga - cash)
  
  if cash >= total_harga
    $("#post_cash").val(total_harga)
    $('#sisa_bayar').html(0)
    $('#post_left_payment').val(0)
    $("#post_status_payment").val("done")
  else if cash < total_harga
    $("#post_status_payment").val("debt")
