class TrashRecording < ApplicationRecord
  belongs_to :inventory
  belongs_to :user, foreign_key: :created_by

  before_save :set_default_value

  paginates_per 10

  def set_default_value
    if self.new_record?
      self.trash_date = Time.now
      self.status     = "active"
      self.is_deleted = false
    end
  end
end
