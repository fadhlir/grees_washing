class InventoryHistory < ApplicationRecord
  belongs_to :inventory
  belongs_to :user, foreign_key: "created_by"

  paginates_per 10

  def self.search(search, start_date, end_date, type)
    if start_date.blank? && end_date.blank?
      start_date = Date.today.beginning_of_month
      end_date = Date.today
    end

    scope = InventoryHistory.joins(:inventory).joins(:user).where("date(inventory_histories.created_at) between ? and ?", start_date, end_date).select("inventories.name, CASE WHEN history_type = 'in' THEN inventory_histories.created_at ELSE null END as in_date, CASE WHEN history_type = 'out' THEN inventory_histories.created_at ELSE null END as out_date, history_type as type, inventories.name as item_name, inventories.code as item_code, inventory_histories.qty as in_out_qty, users.fullname as creator, inventory_histories.employee_name").order("inventory_histories.created_at desc")

    if search.present?
      scope = scope.where("lower(inventories.name) like ?", "%#{search.downcase}%")
    end

    if type.present?
      scope = scope.where("history_type = ?", type)
    end

    return scope
  end
end
