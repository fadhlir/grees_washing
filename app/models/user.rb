class User < ApplicationRecord
  has_secure_password

  belongs_to :role, foreign_key: "role_code"
  has_many :order_invoices, foreign_key: "created_by"
  has_many :order_item_details, foreign_key: "created_by"

  validates :email, presence: true, uniqueness: true
end
