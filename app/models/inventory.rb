class Inventory < ApplicationRecord
  has_many :inventory_histories
  belongs_to :user

  accepts_nested_attributes_for :inventory_histories, allow_destroy: true

  def name_code
    self.name + " | " + self.code
  end

  # params = {
  #   name: "Cairan Racun Mantap",
  #   code: Inventory.get_inventory_code,
  #   qty: 10,
  #   user_id: 1,
  #   inventory_histories_attributes: [{
  #     qty_in: 10,
  #     date_in: Date.today,
  #     history_type: "in",
  #   }]
  # }

  def self.get_inventory_code
    last_inventory      = Inventory.select(:code).order(:id).last
    last_inventory_code = last_inventory.present? ? last_inventory.try(:code).split("OBT").last.to_i : 0
    inventory_number    = last_inventory_code+1
    number_length       = Math.log10(inventory_number).to_i + 1
    code                = "OBT"
    if number_length == 1
      inventory_number = code + "000" + inventory_number.to_s
    elsif number_length == 2
      inventory_number = code + "00" + inventory_number.to_s
    elsif number_length == 3
      inventory_number = code + "0" + inventory_number.to_s
    elsif number_length == 4
      inventory_number = code + inventory_number.to_s
    end
  end

end
