class OrderInvoice < ApplicationRecord
  belongs_to :customer
  has_many :order_item_details, primary_key: "invoice_number", foreign_key: "invoice_number", dependent: :destroy
  has_many :invoice_details
  belongs_to :user, foreign_key: "created_by"

  accepts_nested_attributes_for :order_item_details, :allow_destroy => true

  validates :invoice_number,  :uniqueness => { :case_sensitive => false }
  validates :cash,  :presence => true
  validates :total_invoice,  :presence => true
  before_validation :set_attributes

  paginates_per 10

  def set_attributes
    if self.new_record?
      self.invoice_number   = OrderInvoice.get_invoice_number if self.invoice_number.blank?
      if self.cash.present? && self.total_invoice.present?
        self.cash             = (self.cash >= self.total_invoice) ? self.total_invoice : self.cash
        self.status_payment   = (self.cash >= self.total_invoice) ? "done" : "debt"
      end
      self.invoice_date     = Time.now
      self.due_payment_date = self.invoice_date + 10.days
      self.customer_name    = self.customer_name.downcase if self.customer_name
    end
    self.pay_date = Time.now
  end

  def self.get_invoice_number
    last_invoice        = OrderInvoice.select(:invoice_number).order(:id).last
    last_invoice_number = last_invoice.present? ? last_invoice.try(:invoice_number).split("ORD").last.to_i : 0
    invoice_number      = last_invoice_number+1
    number_length       = Math.log10(invoice_number).to_i + 1
    code                = "ORD"
    if number_length == 1
      invoice_number = code + "000" + invoice_number.to_s
    elsif number_length == 2
      invoice_number = code + "00" + invoice_number.to_s
    elsif number_length == 3
      invoice_number = code + "0" + invoice_number.to_s
    elsif number_length == 4
      invoice_number = code + invoice_number.to_s
    end
  end

  def self.get_total_order(params)
    # params = {
    #   order_invoice: {
    #     customer_name: "Fadhlir",
    #     invoice_number: "",
    #     invoice_date: Time.now,
    #     cash: 50000,
    #     order_item_details_attributes: [{
    #       item_id: 1,
    #       qty: 35,
    #       process_id: 1,
    #       unit_price: 3000,
    #       total_price: 105000
    #     },{
    #       item_id: 2,
    #       qty: 19,
    #       process_id: 1,
    #       unit_price: 14000,
    #       total_price: 266000
    #     }]
    #   }
    # }
    
    order_invoice     = params[:order_invoice]
    order_item_detail = order_invoice[:order_item_details_attributes]
    total_item        = order_item_detail.count
    total_qty_item    = 0
    total_invoice     = 0
    
    order_item_detail.each do |item|
      item[:qty]          = item[:qty].to_i
      item[:unit_price]   = item[:unit_price].to_i
      item[:total_price]  = item[:total_price].to_i
      item[:total_price]  = item[:unit_price] * item[:qty] if (item[:total_price].blank? || item[:total_price] == 0)
      item[:created_by]   = order_invoice[:created_by]

      total_qty_item      = total_qty_item + item[:qty].to_i if item[:qty].present?
      total_invoice       = total_invoice + item[:total_price].to_i
    end

    order_invoice[:cash]            = order_invoice[:cash].to_i
    order_invoice[:total_item]      = total_item
    order_invoice[:total_invoice]   = total_invoice
    order_invoice[:total_qty_item]  = total_qty_item
    order_invoice[:invoice_date]    = Date.today
    order_invoice[:left_payment]    = (order_invoice[:cash].to_i <= total_invoice) ? total_invoice - order_invoice[:cash].to_i : 0
    order_invoice[:change]          = (order_invoice[:cash].to_i >= total_invoice) ? order_invoice[:cash].to_i - total_invoice : 0

    return order_invoice
  end

  def self.search(search=nil, start_date=nil, end_date=nil, status=nil)
    query = ""
    if search.present?
      query = " AND LOWER(customer_name) like ('%#{search.downcase}%') OR invoice_number like ('%#{search.downcase}%')"
    end

    if start_date.present?
      if end_date.blank?
        end_date = start_date
      end
      query += " AND DATE(order_invoices.created_at) between '#{start_date.to_date}' and '#{end_date.to_date}'"
    end

    if status.present?
      if status == "debt"
        query += " AND status_payment = 'debt'"
      elsif status == "done"
        query += " AND status_payment = 'done'"
      end
    end

    scope = OrderInvoice.where("status_payment != 'deleted' #{query}").order("id desc")

  end

  def self.update_order_invoice(oi_id)
    oi = OrderInvoice.find(oi_id)
    item = oi.order_item_details
    total_invoice   = 0
    total_qty_item  = 0
    left_payment    = 0
    total_item      = item.count
    item.each do |detail|
      total_invoice   += detail.total_price
      total_qty_item  += detail.qty      
    end
    left_payment = total_invoice - oi.cash
    oi.update_attributes(total_invoice: total_invoice, total_item: total_item, total_qty_item: total_qty_item, left_payment: left_payment)
  end
  
end
