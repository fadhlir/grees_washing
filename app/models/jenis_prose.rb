class JenisProse < ApplicationRecord
  belongs_to :user, foreign_key: :created_by

  paginates_per 10

  def code_name
    self.jenisproses_code + " | " + self.jenisproses_name
  end

  def self.get_jenis_proses_code
    last_inventory      = JenisProse.select(:jenisproses_code).order(:id).last
    last_inventory_code = last_inventory.present? ? last_inventory.try(:jenisproses_code).split("JP").last.to_i : 0
    inventory_number    = last_inventory_code+1
    number_length       = Math.log10(inventory_number).to_i + 1
    code                = "JP"
    if number_length == 1
      inventory_number = code + "000" + inventory_number.to_s
    elsif number_length == 2
      inventory_number = code + "00" + inventory_number.to_s
    elsif number_length == 3
      inventory_number = code + "0" + inventory_number.to_s
    elsif number_length == 4
      inventory_number = code + inventory_number.to_s
    end
  end
end
