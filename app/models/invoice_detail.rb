class InvoiceDetail < ApplicationRecord
  belongs_to :invoice_master
  belongs_to :order_invoice
end
