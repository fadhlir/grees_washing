class InvoiceMaster < ApplicationRecord
  has_many :invoice_details
  belongs_to :user, primary_key: "id", foreign_key: "created_user_id"
  belongs_to :customer

  accepts_nested_attributes_for :invoice_details, :allow_destroy => true

  validates :invoice_master_code, :uniqueness => { :case_sensitive => false }
  validates :invoice_master_code, :customer_id, :total_invoice, :status_invoice, :created_user_id,  :presence => true

  paginates_per 10

  def set_attributes

  end

  def self.get_invoice_master_number
    last_master         = InvoiceMaster.select(:invoice_master_code).order(:id).last
    last_master_code    = last_master.present? ? last_master.try(:invoice_master_code).split("INV").last.to_i : 0
    invoice_number      = last_master_code+1
    number_length       = Math.log10(invoice_number).to_i + 1
    code                = "INV"
    if number_length == 1
      invoice_number = code + "000" + invoice_number.to_s
    elsif number_length == 2
      invoice_number = code + "00" + invoice_number.to_s
    elsif number_length == 3
      invoice_number = code + "0" + invoice_number.to_s
    elsif number_length == 4
      invoice_number = code + invoice_number.to_s
    end
  end

  def self.generate_invoice(params)
    # params = {
    #   customer_id: 1,
    #   total_invoice: "",
    #   status_invoice: "new",
    #   created_user_id: 1,
    #   invoice_details_attributes: [{
    #     invoice_master_number: "",
    #     order_invoice_id: 47,
    #   },{
    #     invoice_master_number: "",
    #     order_invoice_id: 41,
    #   }]
    # }

    params[:invoice_master_code]  = InvoiceMaster.get_invoice_master_number
    total_invoice                 = 0
    invoice_details               = params[:invoice_details_attributes]

    invoice_details.each do |detail|
      order_invoice                   = OrderInvoice.find(detail[:order_invoice_id])
      total_invoice                   += order_invoice.left_payment if order_invoice.present?
      detail[:invoice_master_number]  = params[:invoice_master_code]
    end

    params[:total_invoice] = total_invoice

    return params
  end

  def self.search(search, start_date, end_date)
    start_date  = start_date.present? ? start_date.to_date : Date.today - 7.days
    end_date    = end_date.present? ? end_date.to_date : Date.today
    scope = InvoiceMaster.joins(:customer).where("DATE(invoice_masters.created_at) between ? and ?", start_date, end_date).select("invoice_masters.id, invoice_master_code, customer_name, invoice_masters.created_at as invoice_date, invoice_masters.total_invoice, status_invoice")

    if search.present?
      search = search.downcase
      scope = scope.where("LOWER(customers.customer_name) like ? or LOWER(invoice_masters.invoice_master_code) like ? or LOWER(status_invoice) like ?", "%#{search}%", "%#{search}%", "%#{search}%")
    end

    return scope
  end

  def self.update_invoice_master(invoice_master_id)
    master        = InvoiceMaster.find(invoice_master_id)
    transactions  = InvoiceMaster.joins("LEFT JOIN invoice_details on invoice_details.invoice_master_id = invoice_masters.id").joins("LEFT JOIN order_invoices on order_invoices.id = invoice_details.order_invoice_id").select("order_invoices.left_payment").where("invoice_masters.id = ?", invoice_master_id)
    left_payment  = 0
    transactions.each do |transaction|
      left_payment += transaction.left_payment
    end
    master.update_attributes(:total_invoice => left_payment)
  end

  def self.make_invoice_done(invoice_master_id)
    master        = InvoiceMaster.find(invoice_master_id)
    transactions  = OrderInvoice.joins("RIGHT JOIN invoice_details on invoice_details.order_invoice_id = order_invoices.id").joins("RIGHT JOIN invoice_masters on invoice_masters.id = invoice_details.invoice_master_id").where("invoice_masters.id = ?", master.id).order(:invoice_number)

    transactions.each do |transaction|
      transaction.order_item_details.update_all(:status_order_item => "completed")
      if transaction.status_payment.eql?("debt")
        pay_date      = Time.now
        total_invoice = transaction.total_invoice
        transaction.update_attributes(:pay_date => pay_date, :cash => total_invoice, :status_payment => "done", :left_payment => 0)
      end
    end

    InvoiceMaster.update_invoice_master(invoice_master_id)
    master.update_attributes(:status_invoice => "done")
  end
end
