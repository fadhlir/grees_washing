class Customer < ApplicationRecord
  has_many :order_invoices
  has_many :invoices

  paginates_per 10

  def code_name
    self.customer_code + " | " + self.customer_name
  end

  def self.get_customer_code
    last_customer        = Customer.select(:customer_code).order(:id).last
    last_customer_code   = last_customer.present? ? last_customer.try(:customer_code).split("CUST").last.to_i : 0
    customer_code        = last_customer_code + 1
    number_length        = Math.log10(customer_code).to_i + 1
    code                 = "CUST"
    if number_length == 1
      customer_code = code + "000" + customer_code.to_s
    elsif number_length == 2
      customer_code = code + "00" + customer_code.to_s
    elsif number_length == 3
      customer_code = code + "0" + customer_code.to_s
    elsif number_length == 4
      customer_code = code + customer_code.to_s
    end
  end

  def self.search(search)
    where("customer_name ILIKE ?", "%#{search}%")
  end

end
