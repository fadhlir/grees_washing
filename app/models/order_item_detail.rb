class OrderItemDetail < ApplicationRecord  
  belongs_to :order_invoice, foreign_key: "invoice_number", primary_key: "invoice_number", optional: true  
  belongs_to :user, foreign_key: "created_by"
  before_save :set_attributes

  paginates_per 10

  def set_attributes
    if self.new_record?
      self.date_entry         = Time.now
      self.date_target_send   = self.date_entry + 10.days
      self.status_order_item  = "progress"
      self.no_si_out          = get_si_out
    end
    if self.id.present?
      detail = OrderItemDetail.find(self.id)
      qty_base  = detail.note.present? ? detail.note.split(" ").last.to_i : detail.qty
      qty_edit  = self.qty
      if qty_base > qty_edit
        self.note         = "Qty barang kurang -#{qty_base - qty_edit} dari total awal #{qty_base}"
        self.total_price  = detail.unit_price * qty_edit
      elsif qty_base < qty_edit
        self.note         = "Qty barang bertambah +#{qty_edit - qty_base} dari total awal #{qty_base}"
        self.total_price  = detail.unit_price * qty_edit
      end
    end
  end

  def get_si_out    
    last_item        = OrderItemDetail.select(:no_si_out).order(:id).last
    code             = "siout"
    
    if last_item.present?
      last_si_out      = last_item.try(:no_si_out).split("siout").last.to_i
      si_out           = last_si_out+1
      number_length       = Math.log10(si_out).to_i + 1
      if number_length == 1
        si_out = code + "000" + si_out.to_s
      elsif number_length == 2
        si_out = code + "00" + si_out.to_s
      elsif number_length == 3
        si_out = code + "0" + si_out.to_s
      elsif number_length == 4
        si_out = code + si_out.to_s
      end
    else
      si_out = code + "0001";
    end
  end

  def self.search(status, search, start_date, end_date, jenis_proses=nil)
    start_date  = start_date.present? ? start_date.to_date : Date.today - 7.days
    end_date    = end_date.present? ? end_date.to_date : Date.today

    scaffold = self.joins(:order_invoice).where("order_item_details.status_order_item = ? AND DATE(order_item_details.date_entry) between ? and ?", status, start_date, end_date).select("order_item_details.id, order_invoices.customer_name, order_invoices.invoice_number as inv_number, order_invoices.status_payment as status_payment, order_item_details.date_entry as date_in, order_item_details.unit_price, order_item_details.total_price as item_total_price, order_item_details.status_order_item, order_item_details.product_name, order_item_details.process_name, order_item_details.qty")
    if search.present?
      scaffold = scaffold.where("order_invoices.invoice_number like ? or LOWER(order_invoices.customer_name) like ? or lower(order_item_details.product_name) like ?", "%#{search}%", "%#{search}%", "%#{search}%")
    end

    if jenis_proses.present?
      process_name = JenisProse.find_by_id(jenis_proses.to_i).try(:jenisproses_name)
      scaffold = scaffold.where("lower(order_item_details.process_name) like ?", "%#{process_name.downcase}%")
    end

    return scaffold.order("id desc")
  end
end
