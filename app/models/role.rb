class Role < ApplicationRecord
  has_many :users
  has_many :role_users

  accepts_nested_attributes_for :role_users, allow_destroy: true

  params_role = {name_role: "rolerolean", role_users_attributes: [{menu_id: 16}, {menu_id: 17}]} 
end
